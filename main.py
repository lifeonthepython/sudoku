"""Sudoku solver module."""

from collections.abc import Iterable
from functools import partial
from itertools import islice

from boards import (
    Board,
    board_with_all_solns,
    board_with_0_solns,
    board_with_1_soln,
    board_with_8_solns,
)


def is_valid(board: Board, r: int, c: int, guess: int) -> bool:
    """Determines if a guess is valid for a board at a given row and column."""
    if guess in board[r]:
        return False

    for row in board:
        if row[c] == guess:
            return False

    top = (r // 3) * 3
    left = (c // 3) * 3
    for br in range(top, top + 3):
        for bc in range(left, left + 3):
            if board[br][bc] == guess:
                return False

    return True


def new_board(board: Board, r: int, c: int, new_value: int) -> Board:
    """Create a board from the given board with a new value at the given location."""
    return [
        [value if (r, c) != (r2, c2) else new_value for c2, value in enumerate(row)]
        for r2, row in enumerate(board)
    ]


def solve(board: Board) -> Iterable[Board]:
    """Yield all solutions to a sudoku board."""
    for r, row in enumerate(board):
        for c, value in enumerate(row):
            if value == 0:
                next_board = partial(new_board, board, r, c)
                for guess in range(1, 10):
                    if is_valid(board, r, c, guess):
                        yield from solve(next_board(guess))
                return None
    yield board


def show(how_many: int, board: Board) -> None:
    """Print a given number of solutions to a board."""
    def print_board():
        for r in board:
            print(r)
        print()

    idx = -1
    print("Solving")
    print("-------")
    print_board()

    for idx, soln in enumerate(islice(solve(board), how_many)):
        msg = f"Solution {idx + 1}"
        print(msg)
        print("-" * len(msg))
        print_board()
    else:
        if idx < 0:
            print("There are no solutions.", end="\n"*2)
        elif idx < how_many - 1:
            print("There are no more solutions.", end="\n"*2)
        else:
            print("There might still more solutions out there...", end="\n"*2)


if __name__ == "__main__":
    show3 = partial(show, 3)
    show3(board_with_0_solns)
    show3(board_with_1_soln)
    show(8, board_with_8_solns)
    show3(board_with_all_solns)
